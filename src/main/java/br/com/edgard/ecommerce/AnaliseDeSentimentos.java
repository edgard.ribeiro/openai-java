package br.com.edgard.ecommerce;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.knuddels.jtokkit.Encodings;
import com.knuddels.jtokkit.api.Encoding;
import com.knuddels.jtokkit.api.EncodingRegistry;
import com.knuddels.jtokkit.api.ModelType;
import com.theokanning.openai.OpenAiHttpException;
import com.theokanning.openai.completion.chat.ChatCompletionRequest;
import com.theokanning.openai.completion.chat.ChatMessage;
import com.theokanning.openai.completion.chat.ChatMessageRole;
import com.theokanning.openai.service.OpenAiService;

public class AnaliseDeSentimentos {
	public static void main(String[] args) throws InterruptedException {
		var arquivosDeAvaliacoes = carregarArquivoDeAvaliacoes();

		for (Path arquivo : arquivosDeAvaliacoes) {

			System.out.println("Iniciando analise do produto: " + arquivo.getFileName());

			var resposta = enviarRequisicao(arquivo);
			salvarArquivoDeAnaliseDeSentimento(arquivo, resposta);

			System.out.println("Analise finalizada.");
		}

	}
	
	private static String enviarRequisicao(Path arquivo) throws InterruptedException {
		var chave = System.getenv("OPENAI_API_KEY");
		var service = new OpenAiService(chave, Duration.ofSeconds(60));

		var promptSistema = "Você é um analisador de sentimentos de avaliações de produtos."
				+ "Escreva um parágrafo com até 50 palavras resumindo as avaliações e depois atribua qual o sentimento geral para o produto."

				+ "Identifique também 3 pontos fortes e 3 pontos fracos identificados a partir das avaliações."

				+ "#### Formato de saída" + "Nome do produto:" + "Resumo das avaliações: [resuma em até 50 palavras]"
				+ "Sentimento geral: [deve ser: POSITIVO, NEUTRO ou NEGATIVO]" + "Pontos fortes: [3 bullets points]"
				+ "Pontos fracos: [3 bullets points]";
		
		var promptUsuario = lerConteudoDoArquivo(arquivo);
		
		String modelo = "gpt-3.5-turbo";
		
		var request = ChatCompletionRequest.builder().model(modelo)
				.messages(Arrays.asList(new ChatMessage(ChatMessageRole.SYSTEM.value(), promptSistema),
						new ChatMessage(ChatMessageRole.USER.value(), promptUsuario)))
				.build();		

		int quantidadeDeTokens = contarTokens(promptUsuario);		

		int tamanhoRespostaEsperada = 2048;

		if (quantidadeDeTokens > 4096 - tamanhoRespostaEsperada) {
			modelo = "gpt-3.5-turbo-16k";
		}

		BigDecimal custo = new BigDecimal(quantidadeDeTokens).divide(new BigDecimal("1000"))
				.multiply(new BigDecimal("0.0010"));
		System.out.println("Custo da requisição: R$" + custo);

		System.out.println("Quantidade de Tokens: " + quantidadeDeTokens);
		System.out.println("Modelo escolhido: " + modelo + "\n");
		
		var segundosProximaTentativa = 5;
		var tentativas = 0;
		while(tentativas++ != 5) {
			try {
				return service.createChatCompletion(request).getChoices().get(0).getMessage().getContent();
			}catch(OpenAiHttpException ex) {
				var errorCode = ex.statusCode;
				
				switch(errorCode) {
					case 401:
						throw new RuntimeException("Erro com a chave de API", ex);
						
					case 429:
						System.out.println("Rate Limits atingido! Nova tentativa em instantes");
						Thread.sleep(1000 * segundosProximaTentativa);
						segundosProximaTentativa *= 2;
						break;
					
					case 500: 
						System.out.println("API fora do ar! Nova tentativa em instantes");
						Thread.sleep(1000 * segundosProximaTentativa);
						segundosProximaTentativa *= 2;
						break;
					
					case 503:
						System.out.println("API fora do ar! Nova tentativa em instantes");
						Thread.sleep(1000 * segundosProximaTentativa);
						segundosProximaTentativa *= 2;
						break;
				}
			}
		}
		throw new RuntimeException("API fora do ar! Tentativa finalizadas sem sucesso!");		
	}	
	
	private static List<Path> carregarArquivoDeAvaliacoes() {
		try {
            var diretorioAvaliacoes = Path.of("src/resources/java/avaliacoes");
            return Files
                    .walk(diretorioAvaliacoes, 1)
                    .filter(path -> path.toString().endsWith(".txt"))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException("Erro ao carregar os arquivos de avaliacoes!", e);
        }		
	}
	
	private static String lerConteudoDoArquivo(Path arquivo) {
        try {
            return Files.readAllLines(arquivo).toString();
        } catch (Exception e) {
            throw new RuntimeException("Erro ao ler conteudo do arquivo!", e);
        }
    }

	private static void salvarArquivoDeAnaliseDeSentimento(Path arquivo, String analise) {
        try {
            var nomeProduto = arquivo
                    .getFileName()
                    .toString()
                    .replace(".txt", "")
                    .replace("avaliacoes-", "");
            var path = Path.of("src/resources/java/analises/analise-sentimentos-" +nomeProduto +".txt");
            Files.writeString(path, analise, StandardOpenOption.CREATE_NEW);
        } catch (Exception e) {
            throw new RuntimeException("Erro ao salvar o arquivo!", e);
        }
    }		

	// Método contador de Tokens
	private static int contarTokens(String prompt) {

		EncodingRegistry registry = Encodings.newDefaultEncodingRegistry();
		Encoding enc = registry.getEncodingForModel(ModelType.GPT_3_5_TURBO);

		return enc.countTokens(prompt);
	}
}
