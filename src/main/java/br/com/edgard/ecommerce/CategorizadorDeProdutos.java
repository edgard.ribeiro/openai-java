package br.com.edgard.ecommerce;

import java.time.Duration;
import java.util.Arrays;
import java.util.Scanner;

import com.theokanning.openai.completion.chat.ChatCompletionRequest;
import com.theokanning.openai.completion.chat.ChatMessage;
import com.theokanning.openai.completion.chat.ChatMessageRole;
import com.theokanning.openai.service.OpenAiService;

public class CategorizadorDeProdutos {

	public static void main(String[] args) {
		
		var leitor = new Scanner(System.in);
		
		System.out.println("Digite as categorias válidas:");
		var categorias = leitor.nextLine();
		
		while(true) {			
			
			System.out.println("\nDigite o nome do produto:");
			var user = leitor.nextLine();			
			 
			var system = "Você é um categorizador de produtos e deve responder apenas o nome da categoria do produto informado"
					+ " Escolha uma categoria dentra a lista abaixo: " 
									
					+ "%s"

					+ "###### exemplo de uso:"

					+ "Pergunta: Bola de futebol"
					+ "Resposta: Esportes"
					
					+ "###### regras a serem seguidas:"
					+ "Caso o usuário pergunte algo que não seja de categorização de produto, "
					+ "voce deve responder que não pode ajudar pois o seu papel é apenas responder a categoria dos produtos"
					+ String.format(categorias);
			
			dispararRequisicao(user, system);
			
		}		
	}

	public static void dispararRequisicao(String user, String system) {

		var chave = System.getenv("OPENAI_API_KEY");
		var service = new OpenAiService(chave, Duration.ofSeconds(30));

		var completionRequest = ChatCompletionRequest.builder().model("gpt-3.5-turbo")
				.messages(Arrays.asList(new ChatMessage(ChatMessageRole.USER.value(), user),
						new ChatMessage(ChatMessageRole.SYSTEM.value(), system)))
				.build();
		service.createChatCompletion(completionRequest).getChoices().forEach(c -> System.out.print(c.getMessage().getContent()));
	}
}
