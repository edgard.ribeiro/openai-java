package br.com.edgard.ecommerce;

import java.math.BigDecimal;

import com.knuddels.jtokkit.Encodings;
import com.knuddels.jtokkit.api.Encoding;
import com.knuddels.jtokkit.api.EncodingRegistry;
import com.knuddels.jtokkit.api.ModelType;

public class ContadorDeTokens {

	public static void main(String[] args) {
		
		EncodingRegistry registry = Encodings.newDefaultEncodingRegistry();
		Encoding enc = registry.getEncodingForModel(ModelType.GPT_3_5_TURBO);
		int qtd = enc.countTokens("Identifique o perfil de compra de cada cliente");
				        
		System.out.println("Quantidade de Tokens: " + qtd);
		
		BigDecimal custo = new BigDecimal(qtd)
				.divide(new BigDecimal("1000"))
				.multiply(new BigDecimal("0.0010"));	
		System.out.println("Custo da requisição: R$" + custo);
		
	}

}
