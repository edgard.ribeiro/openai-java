package br.com.edgard.ecommerce;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Arrays;

import com.knuddels.jtokkit.Encodings;
import com.knuddels.jtokkit.api.Encoding;
import com.knuddels.jtokkit.api.EncodingRegistry;
import com.knuddels.jtokkit.api.ModelType;
import com.theokanning.openai.completion.chat.ChatCompletionRequest;
import com.theokanning.openai.completion.chat.ChatMessage;
import com.theokanning.openai.completion.chat.ChatMessageRole;
import com.theokanning.openai.service.OpenAiService;

public class IdentificadorDePerfil {
	public static void main(String[] args) {
        var promptSistema = 
        		"Identifique o perfil de compra de cada cliente. "
        		
        		
        		+ "A resposta deve ser:"
        		
        		
        		+ "Cliente - descreva o perfil do cliente em três palavras";                  
                

        String clientes = carregarClientesDoArquivo();
        
        int quantidadeDeTokens = contarTokens(clientes);
        
        String modelo = "gpt-3.5-turbo";
        
        int tamanhoRespostaEsperada = 2048;
        
        if(quantidadeDeTokens > 4096 - tamanhoRespostaEsperada) {
        	modelo = "gpt-3.5-turbo-16k";
        }
        
        BigDecimal custo = new BigDecimal(quantidadeDeTokens)
				.divide(new BigDecimal("1000"))
				.multiply(new BigDecimal("0.0010"));	
		System.out.println("Custo da requisição: R$" + custo);
        
        System.out.println("Quantidade de Tokens: " + quantidadeDeTokens);
        System.out.println("Modelo escolhido: " + modelo + "\n");
        
        var request = ChatCompletionRequest
                .builder()
                .model(modelo)
                .maxTokens(tamanhoRespostaEsperada)
                .messages(Arrays.asList(
                        new ChatMessage(
                                ChatMessageRole.SYSTEM.value(),
                                promptSistema),
                        new ChatMessage(
                                ChatMessageRole.SYSTEM.value(),
                                clientes)))
                .build();

        var chave = System.getenv("OPENAI_API_KEY");
        var service = new OpenAiService(chave, Duration.ofSeconds(60));

        System.out.println(
                service
                        .createChatCompletion(request)
                        .getChoices().get(0).getMessage().getContent());
    }

	//Método contador de Tokens
    private static int contarTokens(String prompt) {
    	
    	EncodingRegistry registry = Encodings.newDefaultEncodingRegistry();
		Encoding enc = registry.getEncodingForModel(ModelType.GPT_3_5_TURBO);
		
		return enc.countTokens(prompt);		
	}

    //Método que carrega arquivos no resources
	private static String carregarClientesDoArquivo() {
        try {
            var path = Path.of(ClassLoader
                    .getSystemResource("lista_de_compras_100_clientes.csv")
                    .toURI());
            return Files.readAllLines(path).toString();
        } catch (Exception e) {
            throw new RuntimeException("Erro ao carregar o arquivo!", e);
        }
    }
}
